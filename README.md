# linux-notes

Practical notes for Linux usage

## Getting started

This small note is aimed for scribbling practical command of linux to make the life easier

### Prerequisites

Linux but some notes involve arch linux, some kali!

### Extract icons

To exract icons form ms pe executables we need to install "icoutils" package
Debian/Kali:
```bash
$ sudo apt install icoutils
```

ArchLinux/Manjaro:
```bash
$ sudo pacman -S icoutils
```

Command for extracting the icon:
```bash
$ wrestool -x -t 14 /home/reverser/myms.exe > /home/pictures/extractedicon.ico 
```

### How to extend /tmp directory fs size temporarily

The command below expands to 10G. You can enter any other values
```
$ sudo mount -o remount,size=10G,noatime /tmp
```

### How to cleanup /tmp folder by commandline

```bash
$ sudo rm -rf /tmp/* /tmp/.* &>/dev/null
```

### Browse through workspaces by keyboard combination

To switch the workspace on the right
```bash
<Ctrl>+<Alt>+<Right Arrow> 
```

To switch the workspace on the left
```bash
<Ctrl>+<Alt>+<Left Arrow>
```

### If there is no desktop and right click menu on the desktop

Most probobly the application "xfdesktop" terminated. Enter the command below and don't forget to click the "save session" checkbox after logging out.
```
$ xfdesktop
```

### If xfce desktop icon shadows are wrong

```bash
$ xfcong-query -c xfce4-desktop -p /desktop-icons/center-text -n -t bool -s false
```

### How to add a shared folder from host machine

From edit virtual machine settings, you should enable shared folders and give path for shared folder.
```bash
$ sudo vmhgfs-fuse -o allow_other -o auto_unmount .host:/BOLUS /home/reverser/Shared/BOLUS
```

### How to find file(s) using Linux terminal

```bash
$ find /usr/share -name "filename.ext"
$ find /usr/share -name "*.txt"
$ find /usr/share -name "filename.*"
```

### How to copy content of a file to clipboard from terminal
```bash
1. Method 1:
$ xclip -sel clip < /path/to/myfile.txt

2. Method 2:
# pacman -S xsel

$alias pbcopy='xsel --clipboard --input'
$alias pbpaste='xsel --clipboard --output'
```

### How to downgrade systemd on archlinux
```bash
# pacman -U /var/cache/pacman/pkg/systemd_oldversion.pkg.tar.xz
```
After downgrading you have to run this command:
```bash
# mkinitcpio -p linux
```

### How to fix "Lua: Error during loading" in loading wireshark as superuser
```bash
$ sudo vim /usr/share/wireshark/init.lua
```
Set disable_lua=false to disable_lua=true

### If Lock Screen does not work in panel under "Action Buttons"
First install
```bash
$ sudo pacman -S light-locker
$ xfconf-query -c xfce4-session --create -p /general/LockCommand --set "light-locker-command -l" --type string
```
Then log out from your session and log in back.
To lock your screen from terminal you need to run:
```bash
$ light-locker-command -l
```
And you can also use action buttons on panel.

### How to wipe hdd from commandline/terminal
This will overwrite all partitions, master boot records, and data. Use the sudo command as well (sudo dd...)
Filling the disk with all zeros (This may take a while, as it is making every bit of data 0) :
```bash
$ dd if=/dev/zero of=/dev/sdX bs=1M *replace X with the target drive letter.
```
If you are wiping your hard drive for security, you should populate it with random data rather than zeros (This is going to take even longer than the first example.) :
```bash
$ dd if=/dev/urandom of=/dev/sdX bs=1M *replace X with the target drive letter.
```

### No 'locate' command on Arch Linux
```bash
$ sudo pacman -S mlocate
$ sudo updatedb
```

### To Install Netcat for Arch Linux
```bash
$ sudo pacman -S openbsd-netcat
```

### How to fix "Probing EDD (edd=off to disable)... ok" while booting from USB
While you are on the boot options of Arch Linux, simply press <TAB>. Below you will see booting commandline. Add "edd=off" at the end of line.
```
edd=off
```

### How to use Base64 on terminal
```bash
$ echo  'sctzine.com' | base64
c2N0emluZS5jb20K

$ echo  'c2N0emluZS5jb20K' | base64 --decode
sctzine.com
```
### How to use youtube-dl properly
Download as mp3
```bash
$ youtube-dl VIDEO-URL --extract-audio --audio-format mp3
or
$ youtube-dl VIDEO-URL -x
```
Download as video - automatic version
```bash
$ youtube-dl VIDEO-URL
$ youtube-dl --recorde-video mp4 VIDEO-URL
```
We can list available formats to download
```bash
$ youtube-dl --list-formats VIDEO-URL
```
Sample Output
```
[youtube] vKtwZmhX0lw: Downloading webpage
[youtube] vKtwZmhX0lw: Downloading video info webpage
[youtube] vKtwZmhX0lw: Extracting video information
[youtube] vKtwZmhX0lw: Downloading DASH manifest
[youtube] vKtwZmhX0lw: Downloading DASH manifest
[info] Available formats for vKtwZmhX0lw:
format code  extension  resolution note
171          webm       audio only DASH audio  113k , vorbis@128k (44100Hz), 1.86MiB
140          m4a        audio only DASH audio  128k , m4a_dash container, aac  @128k (44100Hz), 2.14MiB
141          m4a        audio only DASH audio  255k , m4a_dash container, aac  @256k (44100Hz), 4.30MiB
278          webm       180x144    DASH video   63k , webm container, vp9, 1fps, video only, 946.76KiB
160          mp4        180x144    DASH video  112k , avc1.4d400c, 15fps, video only, 1.86MiB
242          webm       300x240    DASH video  170k , vp9, 1fps, video only, 2.50MiB
133          mp4        300x240    DASH video  247k , avc1.4d400d, 25fps, video only, 4.11MiB
243          webm       400x320    DASH video  288k , vp9, 1fps, video only, 4.07MiB
13           3gp        unknown    small 
17           3gp        176x144    small ,  mp4a.40.2, mp4v.20.3
36           3gp        320x240    small ,  mp4a.40.2, mp4v.20.3
5            flv        400x240    small 
43           webm       640x360    medium ,  vorbis, vp8.0
18           mp4        640x360    medium ,  mp4a.40.2, avc1.42001E (best)
```
We can use video format code for downloading the video via -f parameter:
```bash
$ youtube-dl -f 18 VIDEO-URL
```
To download a list of youtube link use -a parameter following path of links file:
```bash
$ youtube-dl -a youtube_links.txt
```
To download a playlist use -i parameter
```bash
$ youtube-dl -i PLAYLIST-URL
$ youtube-dl -i PLAYLIST-ID
```
To download playlist as mp3
```bash
$ youtube-dl -x mp3 -i PLAYLIST-URL 
```

### Compressing using tar and gzip with password
To archive we use tar and to compress we use gzip or bzip2
```bash
$ tar -zcvf archivename.tar.gz DirectoryOrFileName
$ tar -jcvf archivename.tar.bz2 DirectoryOrFileName
```
To encrypt we use gpg
```bash
$ gpg -c -o archivename.tar.gz.gpg archivename.tar.gz
```

### Setting up Vim with syntax on and for python programming
We are going to use `Vundle`. It is a extension manager. Don't install it from AUR, it sucks unfortunately. Install it from github page:
```bash
$ git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```
Now we are going to create .vimrc file under home directory. You can delete or at least back up your .vimrc file if you have one before.
```bash
$ touch .vimrc
```
Below you can see my own `.vimrc` file
```
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Blue DeviL Plugins
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'altercation/vim-colors-solarized'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'Valloric/YouCompleteMe'
" Plugin 'tmhedberg/SimpylFold'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

let python_highlight_all=1
syntax enable
set background=dark
colorscheme solarized

let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
map <C-n> :NERDTreeToggle<CR>

" Blue DeviL rules
set number              " Show line numbers
set autoindent          " Auto-indent new lines
set expandtab           " Use spaces instead of tabs
set shiftwidth=4        " Number of auto-indent spaces
set smartindent         " Enable smart-indent
set smarttab            " Enable smart-tabs
set softtabstop=4       " Number of spaces per Tab
```

YouCompleteMe needs to be installed manually, so enter this directory below and run install.py. It has a cmake dependency.
```bash
$ sudo pacman -S cmake
$ cd .vim/bundle/YouCompleteMe
$ python install.py
```

If you want to add new plugin you can directly paste it under `Blue DeviL Plugins` or open vim then `Plugin 'pluginname'` and enter. Then open up vim and enter `:PluginInstall`, Vundle will automatically get the extension.

Important Reminders:<br />
`Ctrl + n` toggles File/Directory Browser. Also you can use `:NERDTree` command
`Ctrl + w` - `l` or `Ctrl + w` - h will pass between NERDTree file browser and text.

Command Mode `Select`<br />
`V` Selects whole line
`v` Selects one char. So press this and then use arrow keys to select. Also you can use page up/down.
`Ctrl+v` Selects rectangular. Like pressing Alt and selecting in notepad++.

Command Mode `Cut`<br />
`d` Cuts selected. d means delete.

Command Mode `Copy`<br />
`y` Copies selected. y means yank.

Command Mode `Paste`<br />
`P` Pastes before cursor
`p` pastes after cursor

Command Mode `Undo/Redo`<br />
`u` undoes last change.(can be repeated to undo preceedin commands)
`Ctrl + r` undoes last undo

Command Mode `Search and replace all occurences of words`<br />
`%s/foo/bar/g` searches `foo` in every line and replaces with `bar`

Command Mode `Create new file without exiting vim`<br />
While you are in command mode write the code below:
```
:e %h/my_new_filename
```

### Vim `YouCompleteMe` Error Fix:
When you get error like : "The ycmd server SHUT DOWN (restart with ':YcmRestartServer'). Unexpected error while loading the YCM core library. Type ':YcmToggleLogs ycmd_60937_stderr_1ul290yo.log' to check the logs." go to the folder  .vim/bundle/YouCompleme and run "python install.py"
```bash
$ cd .vim/bundle/YouCompleteMe
$ python install.py
```

### How to reverse a binary from commandline
We can need to reverse a binary file immediately while playing ctf's
```bash
$< mybinary xxd -p -c1 | tac | xxd -p -r > reversedbin
```

### How to enable one port : e.g 8080
We can change it by modifying `/etc/httpd/conf/httpd.conf` . This files path can be different on other distributions. Add the below line:
```
Listen 8080
```

### Deleting docker images and freeing some space on disk
The `docker system prune` command will remove all stopped containers, all dangling images, and all unused networks:
```bash
$ sudo docker system prune -a
```
### How to search `text` in a file inside director(ies)y
Using `grep` command of course.
##### Search for text in a single file:
```bash
$ grep text my_file.txt
```
##### Search for text in multiple files:
```bash
$ grep text my_file.txt my_file2.rst my_file3.png
```

##### Search for text in a directory(all files):
`*` - asteriks represents all files
```bash
$ grep text *
```

##### Search for text in a directory and all subdirectories under it:
To search recursively use -r
```bash
$ grep -r text *
```
##### And some importtant tags to use:<br />
`-w` Searches for whole words<br />
`-i` To ignore case sensitivity. As default grep command is case-sensitive<br />
`-v` To exclute something we use v parameter. And we can combine parameters as: `-vi`<br/>
`-c` To count the number of matches<br />
`-l` To List Names of Matching Files

### Using diff command to create patches
A simple way to create patch files is to use `diff` command default in linux.
```bash
$ diff a.txt b.txt
```

A better way is:
```bash
$ diff -Naur retdec-3.3.orig/support/CMakeLists.txt retdec-3.3/support/CMakeLists.txt > SCTZineCMakeLists.patch
```

Let's look at the parameters:<br />
`-r, --recursive` Recursively compare any subdirectories found.<br />
`-a, --text` Treat all files as text.<br />
`-u, -U NUM, --unified[=NUM]` Provide NUM (default 3) lines of unified context.

### Sort lines of a txt file by lenght of the string
For one liners, we can use awk to handle this issue:<br />

Ascending order:
```bash
$ awk '{ printf"%04d|%s\n",length($0),$0}' input.txt|sort|awk -F'|' '{print $2}' > output.txt
or
$ cat input.txt | awk '{ print length, $0 }' | sort -n| cut -d" " -f2-
```

Descending order
```bash
$ awk '{ printf"%04d|%s\n",length($0),$0}' input.txt|sort -r|awk -F'|' '{print $2}' > output.txt
or
$ cat input.txt | awk '{ print length, $0 }' | sort -n -r| cut -d" " -f2-
```

A small python script to do the same:
```python
def sortListViaKeyLengthASC(liste): 
    '''
    returns list with sorting element according to their lengths
    in ascending order
    @param liste: list to be sorted
    @return: returns a list
    '''
    liste.sort(key=len) 
    return liste

def sortListViaKeyLengthDEC(liste): 
    '''
    returns list with sorting element according to their lengths
    in descending order
    @param liste: list to be sorted
    @return: returns a list
    '''
    liste.sort(key=len, reverse=True) 
    return liste

def sortListViaKeyLengthDEC2(liste): 
    '''
    returns list with sorting element according to their lengths
    in descending order. but this time uses another method to first
    order ascending then uses a small list trict to reverse the list.
    @param liste: list to be sorted
    @return: returns a list
    '''
    liste = sortListViaKeyLengthASC(liste)
    return liste[::-1]
```

## Search & Find running processes, then kill them
```bash
$ ps -ef | grep "running_process_name"
$ sudo kill -p "processID"
```

## How to get the list of installed library packages only?
```bash
$ /sbin/ldconfig -p
```

## Deployment

You need debian or debian based kali linux; or you need arch linux or arch linux based manjaro to apply these notes

## Authors

* **Blue DeviL** - *Reverser* - [bluedevil](http://gitlab.com/bluedevil)
* **ErrorInside** - *Reverser* - [errorinside](http://gitlab.com/errorinside)

**HomePage** - [SCTZine](http://www.sctzine.com)

## License

This project is under the MIT License
